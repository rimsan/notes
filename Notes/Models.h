//
//  Models.h
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#ifndef Notes_Models_h
#define Notes_Models_h

#import "User.h"
#import "Note.h"
#import "NTCurrentUser.h"

#endif
