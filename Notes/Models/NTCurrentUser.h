//
//  NTCurrentUser.h
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NTCurrentUser : NSObject

@property (nonatomic,readonly) NSString *userId;
@property (nonatomic,readonly) User *userModel;


+ (NTCurrentUser*)user;
- (void)signInWithUser:(User*)user;
- (void)signUpWithLogin:(NSString*)login;

@end
