//
//  NTCurrentUser.m
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import "NTCurrentUser.h"
#import "NSString+MD5.h"

@implementation NTCurrentUser {
    User *currentUserModel;
}

@synthesize userModel = currentUserModel;


+ (NTCurrentUser*)user {
    static NTCurrentUser *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NTCurrentUser alloc] init];
    });
    return sharedInstance;
}

- (void)signInWithUser:(User*)anUser {
    currentUserModel = anUser;
}

static NSString *salt = @"Hello!";

- (void)signUpWithLogin:(NSString*)login {
    User *user = [User MR_createEntity];
    user.login = login;
    user.userId = [[login stringByAppendingString:salt] MD5];
    [user.managedObjectContext MR_saveToPersistentStoreAndWait];
    currentUserModel = user;
}

- (NSString*)userId {
    return currentUserModel.userId;
}


@end
