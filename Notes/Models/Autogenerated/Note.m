//
//  Note.m
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import "Note.h"
#import "User.h"


@implementation Note

@dynamic title;
@dynamic text;
@dynamic dateCreated;
@dynamic dateUpdated;
@dynamic user;

@end
