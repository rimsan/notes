//
//  User.m
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic login;
@dynamic userId;
@dynamic lastSyncDate;
@dynamic notes;

@end
