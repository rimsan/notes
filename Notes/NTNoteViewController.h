//
//  NTNoteViewController.h
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTNoteViewController : UIViewController

@property (nonatomic,assign) BOOL isEditMode;
@property (nonatomic,strong) Note *note;


@property (nonatomic,weak) IBOutlet UIView *editContainer;
@property (nonatomic,weak) IBOutlet UIView *previewContainer;

@property (nonatomic,weak) IBOutlet UITextField *titleField;
@property (nonatomic,weak) IBOutlet UITextView *textView;

@property (nonatomic,weak) IBOutlet UILabel *previewTitleField;
@property (nonatomic,weak) IBOutlet UITextView *previewTextView;


@end
