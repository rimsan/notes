//
//  NTViewController.h
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTNotesListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate> {
    NSFetchedResultsController *fetchController;
}

@property (nonatomic,weak) IBOutlet UITableView *notesTable;



@end
