//
//  NTViewController.m
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import "NTNotesListViewController.h"
#import "NTNoteViewController.h"

@interface NTNotesListViewController ()

@end

@implementation NTNotesListViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createFetchController];
    
    UIBarButtonItem *syncButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                               target:self
                                                                               action:@selector(syncPressed:)];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                               target:self
                                                                               action:@selector(addNotePressed:)];
    
    self.navigationItem.rightBarButtonItems = @[syncButton,addButton];
    self.title = @"Notes";
    
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self reloadData];
    
}

- (void)reloadData
{
    NSError *error;
	if (![fetchController performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    [self.notesTable reloadData];
}

- (void)createFetchController
{
    fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:[self fetchRequest]
                                                          managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                            sectionNameKeyPath:nil
                                                                     cacheName:nil];
    fetchController.delegate = self;
}

-(void)controller:(NSFetchedResultsController *)controller
  didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath
    forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath;
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.notesTable insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                                  withRowAnimation:UITableViewRowAnimationTop];
            break;
        case NSFetchedResultsChangeDelete:
            [self.notesTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationTop];
            break;
        case NSFetchedResultsChangeUpdate:
            [self.notesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (NSFetchRequest*)fetchRequest
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Note MR_entityName]];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"dateCreated" ascending:NO]];
    request.predicate = [NSPredicate predicateWithFormat:@"user.userId = %@",[NTCurrentUser user].userModel.userId];
    return request;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id  sectionInfo = [[fetchController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Note *note = [fetchController objectAtIndexPath:indexPath];
    cell.textLabel.text = note.title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Note *note = [fetchController objectAtIndexPath:indexPath];
    NTNoteViewController *noteViewController = [[NTNoteViewController alloc] initWithNibName:@"NTNoteViewController" bundle:nil];
    noteViewController.note = note;
    noteViewController.isEditMode = NO;
    [self.navigationController pushViewController:noteViewController animated:YES];
}

- (void)addNotePressed:(id)sender
{
    NTNoteViewController *noteViewController = [[NTNoteViewController alloc] initWithNibName:@"NTNoteViewController" bundle:nil];
    noteViewController.isEditMode = YES;
    [self.navigationController pushViewController:noteViewController animated:YES];
    
}

- (void)syncPressed:(id)sender {
    
}



@end
