//
//  NTLoginViewController.h
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTLoginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *loginField;


- (IBAction)signInPressed:(id)sender;
- (IBAction)signUpPressed:(id)sender;

@end
