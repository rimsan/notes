//
//  NSString+MD5.h
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString*)MD5;

@end
