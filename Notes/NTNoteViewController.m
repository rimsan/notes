//
//  NTNoteViewController.m
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import "NTNoteViewController.h"

@interface NTNoteViewController ()

@end

@implementation NTNoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Note";
    // Do any additional setup after loading the view from its nib.
}

- (void)setIsEditMode:(BOOL)isEditMode
{
    _isEditMode = isEditMode;

    self.editContainer.hidden = !isEditMode;
    self.previewContainer.hidden = isEditMode;
    
    if (isEditMode)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                               target:self
                                                                                               action:@selector(savePressed:)];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                               target:self
                                                                                               action:@selector(editPressed:)];
    }
    
    [self updateViews];
}

- (void)updateViews
{
    self.titleField.text = self.note.title;
    self.textView.text = self.note.text;
    
    self.previewTitleField.text = self.note.title;
    self.previewTextView.text = self.note.text;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateViews];
}

- (void)savePressed:(id)sender
{
    if (nil == self.note)
    {
        self.note = [Note MR_createEntity];
        self.note.dateCreated = [NSDate date];
        self.note.user = [NTCurrentUser user].userModel;
    }
    
    self.note.dateUpdated = [NSDate date];
    self.note.title = self.titleField.text;
    self.note.text = self.textView.text;

    [self.note.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    self.isEditMode = NO;
}

- (void)editPressed:(id)sender
{
    self.isEditMode = YES;
}


@end
