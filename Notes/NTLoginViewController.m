//
//  NTLoginViewController.m
//  Notes
//
//  Created by Roman Petryshen on 7/10/13.
//  Copyright (c) 2013 Roman Petryshen. All rights reserved.
//

#import "NTLoginViewController.h"
#import "NTNotesListViewController.h"

@interface NTLoginViewController ()

@end

@implementation NTLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)signInPressed:(id)sender
{
//    [self presentNotesController];
//    return;
    
    if (![self checkLoginField])
    {
        return;
    }
    
    User *user = [User MR_findFirstByAttribute:@"login" withValue:self.loginField.text];
    
    if (nil == user)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"User not found. Please enter valid login or sign up new one."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
        return;
    }
    
    [[NTCurrentUser user] signInWithUser:user];
    
    [self presentNotesController];
}

- (IBAction)signUpPressed:(id)sender
{
    if (![self checkLoginField])
    {
        return;
    }
    
    User *user = [User MR_findFirstByAttribute:@"login" withValue:self.loginField.text];
    
    if (nil != user)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"User already exists. Please enter another login."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
        return;
    }

    
    [[NTCurrentUser user] signUpWithLogin:self.loginField.text];
    
    [self presentNotesController];
}

- (BOOL)checkLoginField
{
    if (0 == self.loginField.text.length)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                   message:@"Enter login please!"
                                  delegate:nil
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil] show];
        return NO;
    }

    return YES;
}

- (void)presentNotesController {
    NTNotesListViewController *notes = [[NTNotesListViewController alloc] initWithNibName:@"NTNotesListViewController" bundle:nil];
    [self.navigationController pushViewController:notes animated:YES];
}

@end
